/**
 * Created by anhnguyen.
 */
import { normalize } from 'normalizr'
import { camelizeKeys } from 'humps'
import AppConfig from '../Config/AppConfig'
import { HTTP_NO_CONTENT_CODE } from '../Config/Constants'

export default async(endpoint, config, schema) => {
	const fullUrl = (endpoint.indexOf(AppConfig.apiRoot) === -1) ? AppConfig.apiRoot + endpoint : endpoint;
	let response;

	try {
		response = await fetch(fullUrl, config);
	} catch (e) {
		debugger;
		throw new Error('Có lỗi xảy ra, vui lòng kiểm tra kết nối mạng và thử lại!')
	}

	if (response.status === HTTP_NO_CONTENT_CODE) {
		return true;
	}

	let json = await response.json();

	if (!response.ok) {
		throw json.error;
	}

	if (schema) {
		if (json.hasOwnProperty('result') && Array.isArray(json.result)) {
			let {result, metadata} = json;
			const camelizedJson = camelizeKeys(result);
			return Object.assign({metadata},
				normalize(camelizedJson, schema),
			)
		}
		const camelizedJson = camelizeKeys(json);
		return Object.assign({},
			normalize(camelizedJson, schema),
		)
	}
	return json;

}
/**
 * Created by anhnguyen on 12/30/16.
 */
import { createSelectorCreator, defaultMemoize } from 'reselect'
import * as R from 'ramda'

export const createDeepEqualSelector = createSelectorCreator(defaultMemoize, R.equals);
/**
 * Created by anhnguyen on 12/29/16.
 */
import I18n from 'react-native-i18n'

export const parseErrors = errors => {
	const submitErrors = {};
	if (errors.details) {
		const {messages} = errors.details;
		Object.keys(messages).map(field => {
			submitErrors[field] = (I18n.t(messages[field][0]))
		});
	}
	if (Object.keys(submitErrors).length === 0) {
		submitErrors['_error'] = I18n.t(errors.message)
	}
	return submitErrors;
};
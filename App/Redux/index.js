import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import Immutable from 'seamless-immutable'
import { ACCESS_DENIED_CODE } from '../Config/Constants'
import configureStore from './CreateStore'
import rootSaga from './Sagas'

import * as customer from './Customer';
import routers from './Router';

const isAccessDenied = (state = Immutable(false), {code}) => {
	if (code === ACCESS_DENIED_CODE) {
		return Immutable(true);
	}
	return state;
};

export default () => {

	const appReducer = {
		[customer.constants.NAME]: customer.reducer
	};

	/* ------------- Assemble The Reducers ------------- */
	const rootReducer = combineReducers({
		...appReducer,
		routers,
		form: formReducer,
		isAccessDenied
	});

	return configureStore(rootReducer, rootSaga);
}

/**
 * Created by anhnguyen on 12/29/16.
 */
import { ActionConst } from 'react-native-router-flux'
import Immutable from 'seamless-immutable'

const INITIAL_STATE = Immutable({
	scene: {}
});

export default (state = INITIAL_STATE, {type, scene}) => {
	switch (type) {
		case ActionConst.FOCUS:
			return state.merge({scene});
			break;
	}
	return state;
};
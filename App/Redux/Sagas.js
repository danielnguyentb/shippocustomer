/**
 * Created by anhnguyen on 12/29/16.
 */
import { takeEvery, takeLatest } from 'redux-saga'
import { call, put, fork, select } from 'redux-saga/effects'
import { CALL_API } from './ActionTypes'
import { UNAUTHORIZED_CODE } from '../Config/Constants'
import { startSubmit, stopSubmit } from 'redux-form'
import { parseErrors } from '../Lib/ReduxForm'
import fetchApi  from '../Services/Api'

import * as customer from './Customer';

export function* callApi(action) {
	const {types, endpoint, schema, body, method = 'GET', headers, form, ...rest} = action.payload;
	const [requestType, successType, failureType] = types;
	const config = {
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
			...headers,
		},
		method, body: typeof body === 'string' ? body : JSON.stringify(body),
	};

	try {

		const token = yield select(customer.selectors.tokenSelector);
		config.headers.Authorization = token || '';
		yield put({type: requestType});
		if (form) {
			yield put(startSubmit(form))
		}
		const response = yield call(fetchApi, endpoint, config, schema);
		yield put({type: successType, response, ...rest});
		if (form) {
			yield put(stopSubmit(form))
		}
	} catch (error) {
debugger;
		const {code, statusCode} = error;
		yield put({type: failureType, error});
		if (form) {
			yield put(stopSubmit(form, parseErrors(error)))
		}
		if (code == UNAUTHORIZED_CODE || statusCode == UNAUTHORIZED_CODE) {
			// yield put(customer.actions.authClear());
			// yield put(push('/login'));
		}
	}
}

export default function *startApp() {
	yield takeEvery(CALL_API, callApi);
}
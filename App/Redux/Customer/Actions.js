/**
 * Created by anhnguyen on 12/29/16.
 */
import {CALL_API} from '../ActionTypes'
import * as Types from './ActionTypes'

export const fetchApplicationValidation = () => {
	return {
		type: CALL_API,
		payload: {
			types: [Types.APPLICATION_VALIDATION_REQUEST, Types.APPLICATION_VALIDATION_SUCCESS, Types.APPLICATION_VALIDATION_FAILURE],
			endpoint: '/applicationValidations',
			method: 'GET',
		}
	}
};

export const login = (credentials) => {
	return {
		type: CALL_API,
		payload: {
			types: [Types.AUTH_LOGIN_REQUEST, Types.AUTH_LOGIN_SUCCESS, Types.AUTH_LOGIN_FAILURE],
			endpoint: '/customers/login',
			method: 'POST',
			body: JSON.stringify(credentials),
			form: 'UserLoginForm',
		}
	}
};
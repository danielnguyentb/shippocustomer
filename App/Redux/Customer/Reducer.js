/**
 * Created by anhnguyen on 12/29/16.
 */
import { combineReducers } from 'redux'
import Immutable from 'seamless-immutable'
import { UNAUTHORIZED_CODE } from '../../Config/Constants'
import {
	AUTH_LOGIN_SUCCESS, AUTH_LOGIN_FAILURE, AUTH_LOAD_SUCCESS,
	AUTH_LOGIN_REQUEST, AUTH_LOGOUT_SUCCESS, CUSTOMER_CHANGE_PASSWORD_SUCCESS,
	AUTH_UNAUTHORIZED, APPLICATION_VALIDATION_SUCCESS,
	CUSTOMER_SUCCESS, CUSTOMER_CREATE_SUCCESS, CUSTOMER_UPDATE_SUCCESS, CUSTOMER_CREATE_FAILURE,
	DELIVERY_ORDER_CREATE_ADDRESS, DELIVERY_ORDER_LIST_ADDRESS, DELIVERY_ORDER_UPDATE_ADDRESS,
	DELIVERY_ORDER_DELETE_ADDRESS, AUTH_CLEAR
} from './ActionTypes'
import I18n from 'react-native-i18n'
import moment from 'moment'

function customer(customer, action) {
	let {birthday} = customer;
	if (birthday) {
		birthday = new Date(birthday);
		customer['date_birthday'] = moment(birthday).format("D");
		customer['month_birthday'] = moment(birthday).format("M");
		customer['year_birthday'] = moment(birthday).format("Y");
	}
	return customer;
}

const session = (state = Immutable({}), action) => {
	switch (action.type) {
		case AUTH_LOAD_SUCCESS:
			return state.merge(action.session);
		case AUTH_LOGIN_SUCCESS:
			if (action.response) {
				return state.merge(action.response);
			}
			break;
		case AUTH_LOGOUT_SUCCESS:
		case AUTH_CLEAR:
			return Immutable({});
	}
	return state;
};

const isAuthenticated = (state = Immutable(false), action) => {
	const {code, statusCode} = action['error'] || {};
	if ([AUTH_LOAD_SUCCESS, AUTH_LOGIN_SUCCESS].includes(action.type)) return Immutable(true);
	else if ([AUTH_LOGOUT_SUCCESS, AUTH_UNAUTHORIZED, AUTH_CLEAR].includes(action.type)) return Immutable(false);
	else if ([UNAUTHORIZED_CODE].includes(action.code)) return Immutable(false);
	else if (UNAUTHORIZED_CODE == statusCode || UNAUTHORIZED_CODE == code) return Immutable(false);
	return state;
};

const byId = (state = {}, action) => {
	let {response} = action;
	let copy = {...state};
	switch (action.type) {
		case CUSTOMER_SUCCESS:
		case CUSTOMER_CREATE_SUCCESS:
		case CUSTOMER_UPDATE_SUCCESS:
		case DELIVERY_ORDER_CREATE_ADDRESS.SUCCESS:
		case DELIVERY_ORDER_UPDATE_ADDRESS.SUCCESS:
			copy[response.id] = customer(response, action);
			return copy;
		case DELIVERY_ORDER_LIST_ADDRESS.SUCCESS:
			response.map(obj => {
				copy[obj.id] = obj;
			});
			return copy;
		case DELIVERY_ORDER_DELETE_ADDRESS.SUCCESS:
			delete copy[response.id];
			return copy;
	}
	return state;
};

const listAddressId = (state = [], action) => {
	let {response} = action;
	let copy = [...state];
	switch (action.type) {

		case DELIVERY_ORDER_CREATE_ADDRESS.SUCCESS:
			copy.push(response.id);
			return copy;
		case DELIVERY_ORDER_LIST_ADDRESS.SUCCESS:
			return response.map(t => t.id);
		case DELIVERY_ORDER_DELETE_ADDRESS.SUCCESS:
			let arr = [];
			arr = copy.filter(id => id != response.id);
			return arr;
	}
	return state;
};

export const getById = (state, id) => {
	return state.customer.byId[id];
};

export const getListCustomerAddress = (state) => {
	let listAddress = state.customer.listAddressId;
	if (listAddress.length > 0) {
		listAddress.sort((a, b) => {
			if (a == b) return 0;
			return a < b ? 1 : -1;
		});
		return listAddress.map(id => getById(state, id));
	}
};

export default combineReducers({
	session,
	isAuthenticated,
	validation: (state = {}, action) => action.type == APPLICATION_VALIDATION_SUCCESS ? action.response.token : state,
	byId, listAddressId,
	error: (state = null, {error}) => error ? I18n.t(error) : null,
	details: (state = null, action) => action.type == CUSTOMER_CREATE_FAILURE ? action.details : null,
	updatingCustomer: (state = false, {updatingCustomer}) => typeof updatingCustomer === 'boolean' ? updatingCustomer : state,
	updatedCustomer: (state, {type}) => type == CUSTOMER_UPDATE_SUCCESS,
	createdCustomer: (state, {type}) => type == CUSTOMER_CREATE_SUCCESS,
	changedPassword: (state, {type}) => type == CUSTOMER_CHANGE_PASSWORD_SUCCESS,
	loginFailed: (state, {type}) => type == AUTH_LOGIN_FAILURE,
	deleteAddressSuccess: (state, {type}) => type == DELIVERY_ORDER_DELETE_ADDRESS.SUCCESS,
	updatedAddressSuccess: (state, {type}) => type == DELIVERY_ORDER_UPDATE_ADDRESS.SUCCESS,
	createdAddressSuccess: (state, {type}) => type == DELIVERY_ORDER_CREATE_ADDRESS.SUCCESS
})
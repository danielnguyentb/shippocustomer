/**
 * Created by anhnguyen on 12/30/16.
 */
import { createSelector } from 'reselect'
import { NAME } from './Constants'
import { createDeepEqualSelector } from '../../Lib/Selector'
import * as R from 'ramda'

export const customerSelector = state => state[NAME];

export const CustomerByIdSelector = createDeepEqualSelector(
	customerSelector,
	customer => R.memoize(id => customer.byId[id])
);

export const sessionSelector = createDeepEqualSelector(
	customerSelector,
	customer => customer.session
);

export const tokenSelector = createSelector(
	sessionSelector,
	session => session ? session.id : null
);

export const isAuthenticated = createSelector(
	customerSelector,
	customer => customer.isAuthenticated
);

export const getProfileId = createSelector(
	customerSelector,
	customer => {
		if (!customer.isAuthenticated) {
			return null;
		}
		if (!customer.session || !customer.session.userId) {
			return null;
		}
		return customer.session.userId;
	}
);

export const getProfile = createDeepEqualSelector(
	[getProfileId, CustomerByIdSelector],
	(profileId, getCustomer) => getCustomer(profileId)
);
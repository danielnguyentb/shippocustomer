/**
 * Created by anhnguyen on 12/29/16.
 */

import React from 'react'
import {
	View,
	ScrollView,
	Image,
	Keyboard,
	LayoutAnimation,
	StyleSheet
} from 'react-native'
import {
	Icon,
	TextInput,
	Text,
	TouchableOpacity,
	Button
} from '@shoutem/ui'
import TextField from 'react-native-md-textinput'
import { connect } from 'react-redux'
import { Actions as NavigationActions } from 'react-native-router-flux'
import I18n from 'react-native-i18n'
import { login, fetchApplicationValidation } from '../../Redux/Customer/Actions';
import { customerSelector } from '../../Redux/Customer/Selectors'

import { Images, Metrics } from '../../Themes'

class RegisterScreen extends React.Component {

	render() {
		return (
			<Image source={Images.bgFull} style={styles.container}>

				<View style={styles.logoGr}>
					<Image source={Images.logoWhite}/>
				</View>

				<View style={styles.page}>
					<View style={styles.loginStyle}>
						<View>
							<View style={styles.viewInput}>
								<TextField
									label={'Tên Shop'}
									highlightColor={'#c05537'}
									textFocusColor={'#fff'}
									textColor={'#fff'}
									inputStyle={{fontSize:14}}
									labelStyle={{fontSize:13}}
									borderColor={'rgba(255,255,255,0.6)'}
									labelColor={'#fff'}
								/>
								<TextField
									label={'Tên đăng nhập'}
									highlightColor={'#c05537'}
									textFocusColor={'#fff'}
									textColor={'#fff'}
									inputStyle={{fontSize:14}}
									labelStyle={{fontSize:13}}
									borderColor={'rgba(255,255,255,0.6)'}
									labelColor={'#fff'}
								/>
								<TextField
									label={'Email'}
									highlightColor={'#c05537'}
									textFocusColor={'#fff'}
									textColor={'#fff'}
									inputStyle={{fontSize:14}}
									labelStyle={{fontSize:13}}
									borderColor={'rgba(255,255,255,0.6)'}
									labelColor={'#fff'}
								/>
								<TextField
									label={'Điện thoại'}
									highlightColor={'#c05537'}
									textFocusColor={'#fff'}
									textColor={'#fff'}
									inputStyle={{fontSize:14}}
									labelStyle={{fontSize:13}}
									borderColor={'rgba(255,255,255,0.6)'}
									labelColor={'#fff'}
								/>
								<TextField
									label={'Mật khẩu'}
									highlightColor={'#c05537'}
									textFocusColor={'#fff'}
									textColor={'#fff'}
									inputStyle={{fontSize:14}}
									labelStyle={{fontSize:13}}
									labelColor={'#fff'}
									borderColor={'rgba(255,255,255,0.6)'}
									secureTextEntry
								/>
							</View>

						</View>
						<View>
							<Button
								style={{backgroundColor: '#C04C47', width: 140, height: 40,
                        borderRadius:2, marginTop:30, marginBottom:30,
                        borderColor: 'rgba(0,0,0,0.4)',
                        borderWidth: 0.3, }}
								styleName="clear">
								<Icon name="add-friend" style={{color:'#fff'}}/>
								<Text style={{color: 'white', fontSize: 12,}}>ĐĂNG KÝ </Text>
							</Button>
						</View>

					</View>
				</View>

			</Image>
		)
	}

}

const styles = StyleSheet.create({
		container :{
			flex : 1,
			backgroundColor: '#500d73',
			justifyContent: 'space-around',
			flexDirection: 'column',
			width: null,
			height: null,
			resizeMode: 'stretch'
		},
		logoGr :{

			flexGrow: 1,
			justifyContent: 'space-around',
			alignItems: 'center',
		},
		page :{
			flexGrow: 6,
		},
		loginStyle :{
			flex:1,
			justifyContent: 'flex-start',
			alignItems: 'center',

		},
		viewInput : {
			width:300
		},
	}
);

export default connect()(RegisterScreen);

/**
 * Created by anhnguyen on 12/29/16.
 */
import React, { Component } from 'react'
import { Scene, Router } from 'react-native-router-flux'
import { connect } from 'react-redux'
import Styles from '../Navigation/Styles/NavigationContainerStyle'

import LoginScreen from './LoginScreen'
import RegisterScreen from './RegisterScreen'
import NavItems from './NavItems'

const RouterWithRedux = connect()(Router); 

class NavigationRouter extends Component {
	render() {
		return (
			<Router>
				<Scene key='authenticateWrapper' navigationBarStyle={Styles.navBar} titleStyle={Styles.title} leftButtonIconStyle={Styles.leftButton} rightButtonTextStyle={Styles.rightButton}>
					<Scene initial key='login' component={LoginScreen} title='Login' hideNavBar/>
					<Scene key='register' component={RegisterScreen} title='Logout'/>
				</Scene>
			</Router>
		)
	}
}

export default NavigationRouter
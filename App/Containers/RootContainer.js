/**
 * Created by anhnguyen on 12/29/16.
 */
import React, { Component } from 'react'
import { View, StatusBar } from 'react-native'
import NavigationRouter from './NavigationRouter'
import { connect } from 'react-redux'

// Styles
import styles from './Styles/RootContainerStyle'

class RootContainer extends Component {

	render () {
		return (
			<View style={styles.applicationView}>
				<StatusBar barStyle='light-content' />
				<NavigationRouter />
			</View>
		)
	}
}

export default connect()(RootContainer)

/**
 * Created by anhnguyen on 1/4/17.
 */
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
		container: {
			flex: 1,
			backgroundColor: '#500d73',
			justifyContent: 'space-around',
			flexDirection: 'column',
			width: null,
			height: null,
			resizeMode: 'stretch'
		},
		logoGr: {

			flexGrow: 1,
			justifyContent: 'space-around',
			alignItems: 'center',
		},
		page: {
			flexGrow: 6,
		},
		loginStyle: {
			flex: 1,
			justifyContent: 'flex-start',
			alignItems: 'center',

		},
		viewInput: {
			width: 300
		},
	}
);
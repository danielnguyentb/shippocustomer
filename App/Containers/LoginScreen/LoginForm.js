import {
	View,
	ScrollView,
	Image,
	Keyboard,
	LayoutAnimation
} from 'react-native'
import {
	Icon,
	TextInput,
	Text,
	TouchableOpacity,
	Button
} from '@shoutem/ui'
import { Actions as NavigationActions } from 'react-native-router-flux'
import { Field, reduxForm } from 'redux-form'
import TextField from 'react-native-md-textinput'
import React from 'react'

import { Images, Metrics } from '../../Themes'
import styles from './Styles/LoginForm'

class LoginForm extends React.Component {

	render() {

		const {error, submitting, pristine} = this.props;
		return (
			<Image source={Images.bgFull} style={styles.container}>

				<View style={styles.logoGr}>
					<Image source={Images.logoWhite}/>
				</View>

				<View style={styles.page}>
					<View style={styles.loginStyle}>

						<View>
							<View style={styles.viewInput}>

								<TextField
									label={'Tên đăng nhập'}
									highlightColor={'#c05537'}
									textFocusColor={'#fff'}
									textColor={'#fff'}
									inputStyle={{fontSize:14}}
									labelStyle={{fontSize:13}}
									borderColor={'rgba(255,255,255,0.6)'}
									labelColor={'#fff'}
								/>
								<TextField
									label={'Mật khẩu'}
									highlightColor={'#c05537'}
									textFocusColor={'#fff'}
									textColor={'#fff'}
									inputStyle={{fontSize:14}}
									labelStyle={{fontSize:13}}
									labelColor={'#fff'}
									borderColor={'rgba(255,255,255,0.6)'}
									secureTextEntry
								/>
							</View>
						</View>
						<View>
							<Button
								disabled={pristine || submitting}
								style={{backgroundColor: '#C04C47', width: 140, height: 40,
                        borderRadius:2, marginTop:30, marginBottom:30,
                        borderColor: 'rgba(0,0,0,0.4)',
                        borderWidth: 0.3, }}
								styleName="clear">
								<Icon name="ic_user_profile" style={{color:'#fff'}}/>
								<Text style={{color: 'white', fontSize: 12,}}>ĐĂNG NHẬP</Text>
							</Button>

						</View>
						<View>
							<TouchableOpacity onPress={NavigationActions.register}>
								<Text style={{color: 'white', fontSize: 13,}}> Bạn chưa có tài khoản ? Đăng ký</Text>
							</TouchableOpacity>
						</View>

					</View>
				</View>

			</Image>
		);
	}
}

export const LoginReduxForm = reduxForm({
	form: 'UserLoginForm'
})(LoginForm);
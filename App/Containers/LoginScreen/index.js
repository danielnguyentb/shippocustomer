/**
 * Created by anhnguyen on 12/29/16.
 */

import React from 'react'
import { connect } from 'react-redux'
import I18n from 'react-native-i18n'
import { login, fetchApplicationValidation } from '../../Redux/Customer/Actions';
import { customerSelector } from '../../Redux/Customer/Selectors'
import { LoginReduxForm } from './LoginForm'

type LoginScreenProps = {
	dispatch: () => any,
	fetching: boolean
};

class LoginScreen extends React.Component {

	props: LoginScreenProps;

	state: {
		username: string,
		password: string,
		visibleHeight: number,
		topLogo: {
			width: number
		}
	};

	constructor(props: LoginScreenProps) {
		super(props);

	}

	componentWillMount() {
		let {isAuthenticated, dispatch} = this.props;
		if (!isAuthenticated) dispatch(fetchApplicationValidation());
	}

	render() {
		return (
			<LoginReduxForm />
		)
	}

}

const mapStateToProps = (state) => {
	const customer = customerSelector(state);
	return {
		requireCaptcha: customer.validation && customer.validation.captcha,
		validation: customer.validation,
		loginFailed: customer.loginFailed,
		isAuthenticated: customer.isAuthenticated,
		session: customer.session,
		login_error: customer.login_error,
		needChangePass: customer.session.needChangePass,
	}
};

export default connect(mapStateToProps)(LoginScreen);

/**
 * Created by anhnguyen on 12/29/16.
 */

export const HTTP_NO_CONTENT_CODE = 204;
export const RESOURCE_NOT_FOUND_CODE = 404;
export const UNAUTHORIZED_CODE = 401;
export const ACCESS_DENIED_CODE = 403;
// Simple React Native specific changes

export default {
  // font scaling override - RN default is on
  allowTextFontScaling: true,
	apiRoot: 'http://192.168.10.250:3000/api'
};

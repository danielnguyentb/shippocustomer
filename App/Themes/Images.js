// @flow

// leave off @2x/@3x
const images = {
  logo: require('../Images/ir.png'),
  clearLogo: require('../Images/top_logo.png'),
  ignite: require('../Images/ignite_logo.png'),
  tileBg: require('../Images/tile_bg.png'),
  background: require('../Images/BG.png'),
  bgFull: require('../Images/bgfull.png'),
  logoWhite: require('../Images/logowhite.png')
}

export default images
